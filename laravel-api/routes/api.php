<?php

use App\Http\Controllers\PostController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('/post', 'PostController');
Route::apiResource('/comment', 'CommentController');
Route::apiResource('/role', 'RoleController');


Route::group(['middleware' => ['auth:api']], function () {
    // Memproteksi method Store, Update, dan Delete pada PostController dari user yang tidak login
    Route::post('/post/store', 'PostController@Store');
    Route::put('/post/update', 'PostController@update');
    Route::delete('/post/destroy', 'PostController@destroy');

    // Memproteksi method Store, Update, dan Delete pada CommentController dari user yang tidak login
    Route::post('comment/store', 'CommentController@store');
    Route::put('comment/update', 'CommentController@update');
    Route::delete('comment/destroy', 'CommentController@destroy');
});

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth',
], function () {
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');
});

<?php

namespace App\Listeners;

use App\User;
use App\Events\RegisterStored;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationMail;

class SendEmailToRegistrar implements ShouldQueue
{

    public $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  RegisterStored  $event
     * @return void
     */
    public function handle(RegisterStored $event)
    {
        Mail::to($event->user->email)->send(new RegistrationMail($event->user));
    }
}

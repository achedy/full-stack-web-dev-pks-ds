<?php

namespace App\Listeners;

use App\Events\RegeneratedOtpStored;
use App\Mail\RegenerateOtpMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use app\User;

class SendEmailToUser implements ShouldQueue
{
    public $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  RegeneratedOtpStored  $event
     * @return void
     */
    public function handle(RegeneratedOtpStored $event)
    {
        Mail::to($event->user->email)->send(new RegenerateOtpMail($event->user));
    }
}

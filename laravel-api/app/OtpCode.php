<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

class OtpCode extends Model
{
    protected $fillable = ['otp', 'valid_until', 'user_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Str::uuid();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

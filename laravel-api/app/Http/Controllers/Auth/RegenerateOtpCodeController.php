<?php

namespace App\Http\Controllers\Auth;

use App\Events\RegeneratedOtpStored;
// use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
// use App\Mail\RegenerateOtpMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;



class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'   => 'required|email',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if ($user->otp_code) {
            $user->otp_code->delete();
        }

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ($check);

        $valid_until = Carbon::now()->addMinutes(15);
        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $valid_until,
            'user_id' => $user->id
        ]);

        event(new RegeneratedOtpStored($user));

        // Mail::to($request->email)->send(new RegenerateOtpMail($user));

        return response()->json([
            'success' => true,
            'message' => 'OTP user berhasil digenerate',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}

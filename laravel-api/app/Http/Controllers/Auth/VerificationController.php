<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp', $request->otp)->first();

        if (!$otp_code) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code tidak ditemukan',
            ], 404);
        }

        $user = $otp_code->user;

        if ($otp_code->valid_until < Carbon::now()) {
            return response()->json([
                'success' => false,
                'message' => 'OTP Code sudah kedaluwarsa',
            ], 404);
        }

        $user->update([
            'email_verified_at' => Carbon::now(),
        ]);

        $otp_code->delete();

        return response()->json([
            'success' => true,
            'message' => 'Selamat akun anda terverifikasi',
            'data' => $user
        ]);
    }
}

<?php
trait Hewan
{
  protected $nama,
    $darah = 50,
    $jumlahKaki,
    $keahlian;

  public function atraksi()
  {
    $str = $this->nama . " sedang " . $this->keahlian;
    return $str;
  }

  public function getnama()
  {
    return $this->nama;
  }
}

trait Fight
{

  protected $attackPower,
    $defencePower;

  public function getattackPower()
  {
    return $this->attackPower;
  }

  public function serang($hewan)
  {
    $str = "{$this->nama} sedang menyerang {$hewan->getnama()}";
    echo $str;
    echo "<br>";
    echo $hewan->diserang($this);
    echo "<br>";
    return $str;
  }

  public function diserang($hewan)
  {
    $berkurang = ($hewan->getattackPower()) / ($this->defencePower);
    $this->darah -= $berkurang;
    $str = "{$this->nama} sedang diserang {$hewan->getnama()}" . "<br>" .
      "Darah {$this->nama} berkurang : {$berkurang}"  . "<br>" .
      "Sisa Darah {$this->nama} : {$this->darah}";
    return $str;
  }
}

class Elang
{
  use Hewan, Fight;

  public function __construct($nama = '', $jumlahKaki = '2', $keahlian = 'terbang tinggi', $attackPower = 10, $defencePower = 5)
  {
    $this->nama = $nama;
    $this->jumlahKaki = $jumlahKaki;
    $this->keahlian = $keahlian;
    $this->attackPower = $attackPower;
    $this->defencePower = $defencePower;
  }

  public function getInfoHewan()
  {
    $str = "Jenis hewan   : ELANG " . "<br>" .
      "Nama          : " . $this->nama . "<br>" .
      "Darah         : " . $this->darah . "<br>" .
      "Jumlah Kaki   : " . $this->jumlahKaki . "<br>" .
      "Keahlian      : " . $this->keahlian . "<br>" .
      "Attack Power  : " . $this->attackPower . "<br>" .
      "Defence Power : " . $this->defencePower . "<br>";
    return $str;
  }
}

class Harimau
{
  use Hewan, Fight;

  public function __construct($nama = '', $jumlahKaki = '4', $keahlian = 'lari cepat', $attackPower = 7, $defencePower = 8)
  {
    $this->nama = $nama;
    $this->jumlahKaki = $jumlahKaki;
    $this->keahlian = $keahlian;
    $this->attackPower = $attackPower;
    $this->defencePower = $defencePower;
  }

  public function getInfoHewan()
  {
    $str = "Jenis hewan   : HARIMAU " . "<br>" .
      "Nama          : " . $this->nama . "<br>" .
      "Darah         : " . $this->darah . "<br>" .
      "Jumlah Kaki   : " . $this->jumlahKaki . "<br>" .
      "Keahlian      : " . $this->keahlian . "<br>" .
      "Attack Power  : " . $this->attackPower . "<br>" .
      "Defence Power : " . $this->defencePower . "<br>";
    return $str;
  }
}

$elang1 = new elang('Elang Afrika');
echo $elang1->getInfoHewan();
echo "<br /><br />";
$harimau1 = new harimau('Macan Kumbang');
echo $harimau1->getInfoHewan();
echo "<br /><br />";
echo $elang1->atraksi();
echo "<br>";
echo $harimau1->atraksi();
echo "<br /><br />";
$elang1->serang($harimau1);
echo "<br /><br />";
$harimau1->serang($elang1);
echo "<br /><br />";
echo $harimau1->diserang($elang1);
